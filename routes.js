/**
 * Created by guiyep on 10/10/15.
 */
define(['app', 'angularRoute'], function (app) {
    'use strict';

    console.log('app routes loaded');

    return app.config(['$routeProvider', function ($routeProvider) {
        // define route for home
        $routeProvider.when('/home', {
            templateUrl: 'views/home.html'
        });
        // redirect to home if we do not fined the route
        $routeProvider.otherwise({
            redirectTo: '/home'
        });
    }]);
});