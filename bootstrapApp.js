/**
 * Created by guiyep on 10/10/15.
 */
define([
    'require',
    'angular',
    './app',
    './js/controllers/index',
    './js/directives/index',
    './js/services/index',
    './js/model/index',
    './routes'
], function (require, angular) {
    'use strict';

    // load requirejs domReady module
    require(['domReady'], function (domReady) {
        // wait until dom is ready
        domReady(function () {
            // start angular app manually
            angular.bootstrap(document, ['app']);
            // log
            console.log('weather test app finished Loading');
        });
    });
});