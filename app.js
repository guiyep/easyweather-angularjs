/**
 * Created by guiyep on 10/10/15.
 */
define([
    'angular',
    'angularRoute'
], function (angular) {
    'use strict';
    // log
    console.log('app finished loading');
    // return app module initialized
    return angular.module('app', ['ngRoute']);
});