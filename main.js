/**
 * Created by guiyep on 10/10/15.
 */
require.config({
    paths: {
        // requirejs module, that is triggered when dom is ready
        domReady: 'node_modules/domready/ready',
        // angular fwk
        angular: 'node_modules/angular/angular',
        // routing for angular
        angularRoute: 'node_modules/angular-route/angular-route.min'
    },
    shim: {
        angular : {
            exports: 'angular'
        },
        angularRoute: {
            deps: ['angular']
        }
    },
    // here we can load less, but for the porpouse of the test we wont do it.
    deps: ['bootstrapApp', 'node_modules/less/dist/less.min']
});