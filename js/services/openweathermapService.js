/**
 * Created by guiyep on 7/10/15.
 */
define(['../../app', 'angular', '../model/DayWeather'], function (app, angular, DayWeather) {
    'use strict';
    // config new resource
    app.factory('openweathermapService', ['$http', '$q', '$filter', function ($http, $q, $filter) {
        // mode
        var MODE = 'JSON';
        // define app id
        var APP_ID = '515711c91226b81b80a66c21c8d2c415';
        // base url
        var BASE_URL = 'http://api.openweathermap.org/data/2.5/forecast?q={CITY}&appid={APP_ID}';
        // define service
        var serv = {
            // get complete forecaste
            fiveDaysForecast: function(city, op){
                // return promise to the api
                return $http.get(BASE_URL.replace('{CITY}', city), op).then(
                    // success
                    function(response){
                        // last
                        var last = undefined;
                        // create response
                        var list = [];
                        // parse array to get separated data (here is were we translate the response into the model)
                        angular.forEach(response.data.list, function(e){
                            // get date
                            var dateString = $filter('date')(new Date(e.dt_txt + ' UTC'), 'shortDate', (op && op.timezone) ? op.timezone : undefined);
                            // get time
                            var timeString = $filter('date')(new Date(e.dt_txt + ' UTC'), 'shortTime', (op && op.timezone) ? op.timezone : undefined);
                            // validate we have a different time
                            if(last && last.day !== dateString){
                                // if the date is bigger than the current date
                                if(new Date(dateString) > new Date())
                                    // push new day weather to list
                                    list.push(last);
                                // remove last reference
                                last = undefined;
                            }
                            // if we have the same element push new hourly
                            if(last && last.day === dateString){
                                // push item to collection (we know that the response is in F)
                                last.addHourlyTime(timeString, { value : e.main.temp, measure: 'K'})
                            }
                            // validate if we need to add a day weather
                            if(!last){
                                // create a day weather (we know that the response is in F)
                                last = new DayWeather(dateString);
                                // push item to collection
                                last.addHourlyTime(timeString, { value : e.main.temp, measure: 'K'})
                            }
                        });
                        // append last element if any (since this api return 5 days in UTC we may have some days splitted, we need
                        // to set the array to show only 5 otherwise we will have imcoplete data)
                        if(last && last.length < 5) {
                            // push item to the collection
                            list.push(last);
                            // remove last reference
                            last = undefined;
                        }
                        // return pared value for this city
                        return { data : list };
                    },
                    // on error
                    function(error){
                        // return rejected promise
                        return $q.reject(error);
                    }
                );
            }
        };
        // return service object
        return function(baseUrl, mode, appId){
            // create url
            BASE_URL = (baseUrl ? baseUrl : BASE_URL).replace('{APP_ID}', appId ? appId : APP_ID);
            // retunr service
            return serv;
        }
    }]);
});