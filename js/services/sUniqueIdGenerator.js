/**
 * Created by guiyep on 12/10/15.
 */
define(['../../app'], function (app) {
    'use strict';
    // config new resource
    app.factory('sUniqueIdGenerator', [function () {
        // return service function with a unique id
        return function (variable) {

            var delim = variable ? '' : '-';

            function S4() {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            }

            return (variable ? '_' : '') +(S4() + S4() + delim + S4() + delim + S4() + delim + S4() + delim + S4() + S4() + S4());
        };
    }]);
});