/**
 * Created by guiyep on 10/10/15.
 */
define(['../../app'], function (app) {
    'use strict';
    // create the application controller
    app.controller('weatherController', ['$scope', function($scope) {
        //  temp values
        var tempSettings = {
            celcius : 'C',
            farenheit : 'F',
            kelvin: 'K'
        };
        // create function that update the model based on the metrics
        $scope.setValueWithSettings = function(value, fromSetting, toSetting){
            // TODO, HERE WE CAN DEFINE ALL THE POSSIBLE CONVERSIONS
            // return value based on setting
            if(fromSetting == tempSettings.kelvin && toSetting == tempSettings.farenheit){
                // get value
                var value = Math.floor(value);
                // parsed value
                var parsed = ((value * 1.8 )- 495);
                // return value
                return value === 0 ? 0 : Math.floor(parsed) + ' ' + tempSettings.farenheit;
            }
            // return value based on setting
            if(fromSetting == tempSettings.kelvin && toSetting == tempSettings.celcius){
                // get value
                var value = Math.floor(value);
                // parsed value
                var parsed = (value - 273 );
                // return value
                return value === 0 ? 0 : Math.floor(parsed) + ' ' + tempSettings.celcius;
            }
            // return undefined otherwise
            return undefined;
        }
    }]);
});