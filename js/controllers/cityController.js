/**
 * Created by guiyep on 11/10/15.
 */
define(['../../app'], function (app) {
    'use strict';
    // create the application controller
    app.controller('cityController', ['$scope', '$http', '$filter', '$q', function($scope, $http, $filter, $q) {
        // save contries ref
        var list;
        // define city options for autocomplete
        $scope.options = {
            // text field
            dataTextField: 'name',
            // value field
            dataValueField: 'value',
            // define datasource
            dataSource : function(val) {
                // get cities and parse to a known result
                return (list ? $q.when(list) : $http.get('countriesToCities.json', {})).then(
                    // success
                    function(response){
                        // validate if we alreay have a list
                        if(!list) {
                            // cities
                            var cities = [];
                            // parse response
                            for (var prop in response.data) {
                                // extend array
                                angular.forEach(response.data[prop], function (e) {
                                    // push the new item (save city name to have a fast access)
                                    cities.push({ name: e + ',' + prop, value: (e + ',' + prop), cityName: e});
                                });
                            }
                            // save list
                            list = cities;
                        }
                        // coincidence
                        var newList = $filter('filter')(list, { cityName : val});
                        // no duplicates
                        var noDuplicates = [];
                        // remove duplicates (THIS JSON HAS THE PROBLEM OF DUPLICATE CITIES)
                        angular.forEach(newList, function(e){
                            // validate is duplicate, and add it if is not
                            if($filter('filter')(noDuplicates, e).length === 0)
                                noDuplicates.push(e);
                        });
                        // return list without duplicates
                        return noDuplicates;
                    },
                    // error
                    function(){
                        $log.error('Error loading google places')
                    }
                );
            }
        }
    }]);
});