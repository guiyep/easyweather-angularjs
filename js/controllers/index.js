/**
 * Created by guiyep on 10/10/15.
 */
define([
    './appController',
    './cityController',
    './weatherController'
], function () {
    // validate console
    if(window.console && console.log)
        console.log('angular controllers were loaded correctly');
});