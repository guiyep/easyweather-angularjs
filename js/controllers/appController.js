/**
 * Created by guiyep on 10/10/15.
 */
define(['../../app'], function (app) {
    'use strict';
    // create the application controller
    app.controller('appController', ['$scope', 'openweathermapService', '$timeout', '$http', '$log', '$window', function($scope, openweathermapService, $timeout, $http, $log, $window) {
        // define model
        $scope.model = {
            tempMeasure : 'F',
            city: undefined
        };
        // define options for list
        $scope.options = {
            // template
            template: 'template/weather.html'
        };
        // set locating indicator
        $scope._locating = true;

        $http.get('https://ipinfo.io/geo').then(function(response) {
            var loc = response.loc.split(',');
            var coords = {
                latitude: loc[0],
                longitude: loc[1]
            };

            alert(coords.latitude + ' ' +coords.longitude);
        });

        // execute get current position
        $http.get('https://ipinfo.io/geo').then(
            // on success
            function(response){
                var loc = response.data.loc.split(',');
                var coords = {
                    latitude: loc[0],
                    longitude: loc[1]
                };
                // broadcast event
                $scope.$broadcast(app.sLoadingEvents.show, 'Locating your position...');
                // get city name based on lat long from google api
                $http.get('http://maps.googleapis.com/maps/api/geocode/json?latlng='+ (coords.latitude + ',' + coords.longitude) +'&sensor=true').then(
                    // on success
                    function(response){
                        // parse city name from google api response
                        var cityName = response.data.results[2].address_components[0].long_name + ',' + response.data.results[2].address_components[2].long_name;
                        // apply changes so everyone know that we have updated the model;
                        $timeout(function () {
                            // set city
                            $scope.model.city = cityName;
                            // set locating indicator
                            $scope._locating = false;
                            // broadcast event
                            $scope.$broadcast(app.sLoadingEvents.hide);
                        });
                    },
                    // on error
                    function(error){
                        // log error
                        $log.error(error);
                        // broadcast event
                        $scope.$broadcast(app.sLoadingEvents.hide);
                    }
                );

            }
        );
        // load forecast service
        var forecastService =  openweathermapService();
        // set watch over name
        var unregister = $scope.$watch('model.city', function(value){
            // validate we have a value
            if(value) {
                // broadcast event
                $scope.$broadcast(app.sLoadingEvents.show, 'Loading City...');
                // get timezone from api

                $http.get('http://api.worldweatheronline.com/premium/v1/tz.ashx?key=d1c411e6e1ba4fbd8c9222022171012&q='+ value +'&format=json').then(
                    // on success
                    function(response){
                        // get local time
                        var localtime = response.data.data.time_zone[0].localtime;
                        // parse value from response
                        var offset = parseFloat(response.data.data.time_zone[0].utcOffset);
                        // get decimal part
                        var f = offset - parseInt(offset);
                        // get representative timezone floor, 60 minutes is 1 hour
                        var fTimezone = f * 60;
                        // parse offset from api to service
                        var timezone = (offset > 0 ? '+' : '') + parseInt(offset).toString().replace('.', '') + fTimezone + (fTimezone.toString().length > 1 ? '' : '0');
                        // get data from service
                        forecastService.fiveDaysForecast(value, { timezone :  timezone}).then(
                            // success
                            function (response2) {
                                // apply changes so everyone know that we have updated the model;
                                $timeout(function () {
                                    // set selected and local time
                                    $scope._selected = value;
                                    // set local time
                                    $scope._localtime = localtime;
                                    // save weather
                                    $scope.model.forecast = response2.data;
                                    // broadcast event
                                    $scope.$broadcast(app.sLoadingEvents.hide);
                                });
                            },
                            // error
                            function (error) {
                                // alert error
                                $log.error(error);
                                // broadcast event
                                $scope.$broadcast(app.sLoadingEvents.hide);
                            }
                        );
                    },
                    // on error
                    function(error){
                        // alert error
                        $log.error(error);
                        // broadcast event
                        $scope.$broadcast(app.sLoadingEvents.hide);
                    }
                );

            }
        });
        // ondestroy
        var unDestroy = $scope.$on('$destroy', function(){
            // remove listeners
            unregister();
            unDestroy();
        })
    }]);
});