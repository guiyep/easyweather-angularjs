/**
 * Created by guiyep on 10/10/15.
 */
define(['../../app'], function (app) {
    'use strict';

    app.sItemTemplateEvents = {
        finished : 'sItemTemplate-finished'
    };

    app.directive('sItemTemplate', ['$compile', '$parse', '$sce', '$templateRequest', '$q', function ($compile, $parse, $sce, $templateRequest, $q) {
        return {
            // restrict to class name
            restrict: 'C',

            link: function (scope, element, attrs) {
                // create parser for model
                var getter = $parse(attrs.ngModel ? attrs.ngModel : 'item');
                // extend properties
                angular.extend(scope, getter(scope));
                // Make sure that no bad URLs are fetched. You can omit this if your template URL is not dynamic.
                var templateUrl = $sce.getTrustedResourceUrl(scope.options.template);
                // defer execution
                var deferred = $q.defer();
                // wait until request is finished
                $templateRequest(templateUrl).then(
                    // on success promise
                    function(template) {
                        // create template
                        var aTemplate = document.createElement('div');
                        // set html
                        aTemplate.innerHTML = template;
                        // append element
                        element.append(aTemplate);
                        // compile to parent scope new item
                        $compile(aTemplate)(scope);
                        // finish
                        deferred.resolve(scope.options.template)
                    },
                    // on error promise
                    function(error) {
                        /// finish
                        deferred.reject(error);
                        // retunr rejeted promise
                        throw new Error(JSON.stringify(error));
                    }
                );
                // emit promise to know when is finished
                scope.$emit(app.sItemTemplateEvents.finished, deferred.promise);
            }
        };
    }]);
});