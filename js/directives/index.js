/**
 * Created by guiyep on 10/10/15.
 */
define([
    './s-autocomplete',
    './s-list',
    './s-item-template',
    './s-loading'
], function () {

    // validate console
    if(window.console && console.log)
        console.log('angular directives were loaded correctly');
});