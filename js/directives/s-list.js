/**
 * Created by guiyep on 10/10/15.
 */
define(['../../app'], function (app) {
    'use strict';

    app.directive('sList', ['$document', '$compile', function ($document, $compile) {

        return {

            // restrict to just class, we reduce the css problems
            restrict: 'C',

            require: 'ngModel',

            link: function(scope, element, attr){
                // check if we have a tempalte
                if(angular.isUndefined(scope.options) || (scope.options && angular.isUndefined(scope.options.template)))
                    // throw exception
                    throw new Error('need a template to be set in the directive options');
                // create element
                var repeater = $document[0].createElement('div');
                // set inner html
                repeater.innerHTML = '<div ng-repeat="item in '+attr.ngModel+'">' +
                    '<div class="sItemTemplate" bc-init="options = { template: \'' + scope.options.template +'\' } "></div>' +
                '</div>';
                // append to element
                element[0].appendChild(repeater);
                // compile element to scope
                $compile(repeater)(scope);
            }
        };
    }]);
});