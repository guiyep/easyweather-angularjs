/**
 * Created by guiyep on 11/10/15.
 */
define(['../../app'], function (app) {
    'use strict';

    app.directive('sAutocomplete', ['$compile', '$q', '$filter', '$log', function ($compile, $q, $filter, $log) {
        return {
            // restrict to class name
            restrict: 'C',

            require: 'ngModel',

            template: '<input ng-change="searchItem(__searchText)" type="text" ng-model="__searchText">'  +
                    '<select ng-if="itemsCollection">' +
                        '<option class="op" value="{{item[options.dataValueField]}}" ng-repeat="item in itemsCollection">{{item[options.dataTextField]}}</option>' +
                    '</select>',

            controller: ['$scope', function($scope){
                // define search function
                var __search = function(val) {
                    // return the result of the promise or array
                    return $q.when(typeof $scope.options.dataSource == 'function' ? $scope.options.dataSource(val) : $filter('filter')($scope.options.dataSource, val)).then(
                        // on success
                        function(data){
                            // return result
                            return data;
                        }
                    )
                };
                // on value selected update search
                $scope.valueSelected = function(value){
                    // on value selected update text
                    $scope.__searchText = value;
                };
                // search item
                $scope.searchItem = function(value){
                    // validate value
                    if(value && value.length > $scope.options.min) {
                        // search items and update model
                        __search(value).then(
                            // on success
                            function (data) {
                                // update model
                                $scope.itemsCollection = (data && data.length) > 0 ? data : undefined;
                            },
                            // on error
                            function (error) {
                                // log error
                                $log.error(error);
                            }
                        );
                    }
                    else {
                        // remove items
                        $scope.itemsCollection = undefined;
                    }
                };
            }],

            compile: function(tElements, tAttrs){
                // set attribute
                tElements[0].childNodes[1].setAttribute('ng-model', tAttrs.ngModel);
                // pass the model here
                tElements[0].childNodes[1].setAttribute('ng-change', 'valueSelected('+tAttrs.ngModel+')');
                // return link function
                return function(scope, element, attr, ngModel){
                    // throw error if we do not have any datasource set
                    if(angular.isUndefined(scope.options) || scope.options && angular.isUndefined(scope.options.dataSource))
                    // thow error
                        throw new Error('s-autocomplete needs datasource to be set');
                    // validate text field and value field
                    if(scope.options && angular.isUndefined(scope.options.dataValueField))
                    // thow error
                        throw new Error('s-autocomplete needs value field to be set');
                    // validate text field
                    if(scope.options && angular.isUndefined(scope.options.dataTextField))
                    // thow error
                        throw new Error('s-autocomplete needs text field to be set');
                    // if we do not have any min set 4 as default
                    scope.options.min = scope.options.min ? scope.options.min : 4;
                    // set render function
                    ngModel.$render = function(){
                        // on value selected update text
                        scope.__searchText = ngModel.$viewValue;
                        // validate value
                        scope.searchItem(ngModel.$viewValue);
                    };
                }
            }
        };
    }]);
});