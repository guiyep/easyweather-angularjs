/**
 * Created by guiyep on 12/10/15.
 */
define(['../../app'], function (app) {
    'use strict';

    app.sLoadingEvents = {
        show : 'sLoading-show',
        hide: 'sLoading-hide'
    };

    app.directive('sLoading', ['sUniqueIdGenerator', function (sUniqueIdGenerator) {

        return {

            // restrict to just class, we reduce the css problems
            restrict: 'C',

            scope: {

            },

            replace: true,

            template: '<div ng-show="_showIndicator">{{_label}}</div>',

            controller: ['$scope','$attrs', function($scope, $attrs){
                // initialize label
                var defaultlabel = 'Loading...';
                    // lsiten to event
                var unregisterShow = $scope.$on(app.sLoadingEvents.show, function(e, label, id){
                    // if defualt not prevented
                    if(!e.defaultPrevented){
                        // validate if we are the same id
                        if(angular.isUndefined(id) || ($attrs.id == id)) {
                            // set as shown
                            $scope._showIndicator = true;
                            // show indicator
                            $scope._label = label ? label : defaultlabel;
                        }
                    }
                    // validate we have to stop propagation
                    if(e.stopPropagation)
                        // cancel propagation
                        e.stopPropagation();
                    // validate prevent default
                    if(e.preventDefault)
                        // prevent default
                        e.preventDefault()

                });
                // lsiten to event
                var unregisterHide =  $scope.$on(app.sLoadingEvents.hide, function(e, id){
                    // if defualt not prevented
                    if(!e.defaultPrevented){
                        // validate if we are the same id
                        if(angular.isUndefined(id) || ($attrs.id == id)) {
                            // set as shown
                            $scope._showIndicator = false;
                            // show indicator
                            $scope._label = defaultlabel;
                        }
                    }
                    // validate we have to stop propagation
                    if(e.stopPropagation)
                    // cancel propagation
                        e.stopPropagation();
                    // validate prevent default
                    if(e.preventDefault)
                    // prevent default
                        e.preventDefault()
                });

                // lsiten to destroy
                var unregisterDestroy =  $scope.$on('$destroy', function(){
                    // unregister events
                    unregisterShow();
                    unregisterHide();
                    unregisterDestroy();
                });
            }],

            link: function(scope, element, attrs){
                // set attribute
                element[0].setAttribute('id', attrs.id ? attrs.id : sUniqueIdGenerator())
            }
        };
    }]);
});