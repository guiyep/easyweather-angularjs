/**
 * Created by guiyep on 10/10/15.
 */
define(['./HourlyWeather'], function(HourlyWeather){
    // return day Weather object
    return function DayWeather(day){
        // validate day ans weather
        if( !typeof day =='string')
            // thow exception
            throw new Error('invalid parameters passed to dat Weather class');
        // set hourly
        this.day = day;
        // set hourly list
        this.hourly = [];
        // create function
        function _addHourlyTemp(time, temp) {
            // push new item
            this.hourly.push(new HourlyWeather(time, temp))
        }
        // expose method
        this.addHourlyTime = _addHourlyTemp
    }
});