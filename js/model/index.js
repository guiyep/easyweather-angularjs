/**
 * Created by guiyep on 10/10/15.
 */
define([
    './DayWeather',
    './HourlyWeather'
], function () {

    // validate console
    if(window.console && console.log)
        console.log('angular directives were loaded correctly');
});