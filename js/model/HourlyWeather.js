/**
 * Created by guiyep on 10/10/15.
 */
define([], function(){
    // return day Weather object
    return function HourlyWeather(time, temperature){
        // validate day ans weather
        if( !typeof time === 'string' ||  !typeof temperature === 'object')
        // thow exception
            throw new Error('invalid parameters passed to dat Weather class');
        // set time
        this.time = time;
        // set temperature
        this.temp = {
            val : temperature.value,
            measure: temperature.measure
        };
    }
});